Source: libmemcached
Priority: optional
Maintainer: Ondřej Surý <ondrej@debian.org>
Uploaders: Michael Fladischer <fladi@debian.org>
Build-Depends: bison,
               cmake (>= 3.12.0),
               debhelper-compat (= 13),
               flex,
               libevent-dev,
               libsasl2-dev,
               libssl-dev,
               pkg-config,
               python3-sphinx
Standards-Version: 4.5.1
Section: libs
Homepage: https://awesomized.github.io/libmemcached/
Vcs-Git: https://salsa.debian.org/debian/libmemcached.git
Vcs-Browser: https://salsa.debian.org/debian/libmemcached

Package: libmemcached11
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends},
         ${shlibs:Depends}
Description: C and C++ client library to the memcached server
 libmemcached is a C and C++ client library to the memcached server
 (http://memcached.org/). It has been designed to be light on memory
 usage, thread safe, and provide full access to server side methods.
 .
 A few notes on its design:
 .
   * Synchronous and Asynchronous support.
   * Access to large object support.
   * Local replication.
   * TCP and Unix Socket protocols.
   * A half dozen or so different tuneable hash algorithms.
   * Implementations of the new cas, replace, and append operators.
   * Man pages written up on entire API.
   * Implements both modulo and consistent hashing solutions.
   * Tools to Manage your Memcached networks.
 .
 This package provides the libmemcached shared library itself.

Package: libmemcached-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libhashkit-dev (=${binary:Version}),
         libmemcached11 (= ${binary:Version}),
         libmemcachedutil2 (= ${binary:Version}),
         libsasl2-dev,
         ${misc:Depends},
         ${shlibs:Depends}
Description: C and C++ client library to the memcached server (development files)
 libmemcached is a C and C++ client library to the memcached server
 (http://memcached.org/). It has been designed to be light on memory
 usage, thread safe, and provide full access to server side methods.
 .
 A few notes on its design:
 .
   * Synchronous and Asynchronous support.
   * Access to large object support.
   * Local replication.
   * TCP and Unix Socket protocols.
   * A half dozen or so different tuneable hash algorithms.
   * Implementations of the new cas, replace, and append operators.
   * Man pages written up on entire API.
   * Implements both modulo and consistent hashing solutions.
   * Tools to Manage your Memcached networks.
 .
 This package provides the development files.

Package: libmemcachedutil2
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: libmemcached11 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Description: library implementing connection pooling for libmemcached
 libmemcached is a C and C++ client library to the memcached server
 (http://memcached.org/). It has been designed to be light on memory
 usage, thread safe, and provide full access to server side methods.
 .
 A few notes on its design:
 .
   * Synchronous and Asynchronous support.
   * Access to large object support.
   * Local replication.
   * TCP and Unix Socket protocols.
   * A half dozen or so different tuneable hash algorithms.
   * Implementations of the new cas, replace, and append operators.
   * Man pages written up on entire API.
   * Implements both modulo and consistent hashing solutions.
   * Tools to Manage your Memcached networks.
 .
 This package provides the libmemcachedutil library.

Package: libhashkit2
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends},
         ${shlibs:Depends}
Description: libmemcached hashing functions and algorithms
 libhashkit is a small and thread-safe client library that provides a
 collection of useful hashing algorithm. libhashkit is distributed with
 libmemcached.
 .
 This package provides the libhashkit shared library.

Package: libhashkit-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Replaces: libhashkit2 (<< 1.1.3-1~),
          libmemcached-dev (<< 1.0.3-1)
Breaks: libhashkit2 (<< 1.1.3-1~),
        libmemcached-dev (<< 1.0.3-1)
Depends: libhashkit2 (=${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Description: libmemcached hashing functions and algorithms (development files)
 libhashkit is a small and thread-safe client library that provides a
 collection of useful hashing algorithm. libhashkit is distributed with
 libmemcached.
 .
 This package provides the development files.

Package: libmemcached-tools
Section: utils
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends}
Description: Commandline tools for talking to memcached via libmemcached
 libmemcached is a C and C++ client library to the memcached server
 (http://memcached.org/). It has been designed to be light on memory
 usage, thread safe, and provide full access to server side methods.
 .
 This package provides several command line tools:
 .
   * memccat - Copy the value of a key to standard output
   * memcflush - Flush the contents of your servers.
   * memcrm - Remove a key(s) from the server.
   * memccp - Copy files to a memcached server.
   * memcstat - Dump the stats of your servers to standard output
   * memcslap - Generate testing loads on a memcached cluster
